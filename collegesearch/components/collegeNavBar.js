import { Navbar, Nav, NavDropdown } from 'react-bootstrap'

export default function CollegeNavBar() {
    return (
        <div>
            <Navbar bg="dark" variant="dark">
                <Navbar.Brand href="/">collegesearch.me</Navbar.Brand>
                <Nav>
                    <Nav.Link href="/about">About</Nav.Link>
                    <NavDropdown title="See All">
                        <NavDropdown.Item href="/colleges">Colleges</NavDropdown.Item>
                        <NavDropdown.Item href="/cities">Cities</NavDropdown.Item>
                        <NavDropdown.Item href="/admissions">Admissions</NavDropdown.Item>
                    </NavDropdown>
                    <Nav.Link href="/creators">Creators</Nav.Link>
                </Nav>
            </Navbar>
        </div>
    )
}