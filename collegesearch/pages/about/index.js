import dynamic from "next/dynamic"

const CollegeNavBar = dynamic(
    () => import('../../components/collegeNavBar')
)

export default function About() {
    return (
        <div>
            <CollegeNavBar></CollegeNavBar>
            <p>this is where we talk about what is site is for</p>
        </div>
    )
}