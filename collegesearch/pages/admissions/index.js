import dynamic from "next/dynamic"

const CollegeNavBar = dynamic(
    () => import('../../components/collegeNavBar')
)

export default function AdmissionRates() {
    return (
        <div>
            <CollegeNavBar></CollegeNavBar>
            <div className="container" style={{marginTop:"5vh"}}>
                <div className="row justify-content-center">
                    <h1>Admissions</h1>
                </div>
                <div className="row justify-content-center">
                    <a href="/admissions/utaustin">University of Texas at Austin</a>
                </div>
                <div className="row justify-content-center">
                    <a href="/admissions/mit">Massachusetts Institute of Technology</a>
                </div>
                <div className="row justify-content-center">
                    <a href="/admissions/caltech">California Institute of Technology</a>
                </div>
            </div>
        </div>
    )
}