import dynamic from "next/dynamic"

const CollegeNavBar = dynamic(
    () => import('../../components/collegeNavBar')
)

export default function Colleges() {
    return (
        <div>
            <CollegeNavBar></CollegeNavBar>
            <div className="container" style={{marginTop:"5vh"}}>
                    <h1>Massachusetts Institute of Technology</h1>
            </div>
        </div>
    )
}