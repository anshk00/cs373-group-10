import dynamic from "next/dynamic"

const CollegeNavBar = dynamic(
    () => import('../../components/collegeNavBar')
)

export default function Colleges() {
    return (
        <div>
            <CollegeNavBar></CollegeNavBar>
            <div className="container" style={{marginTop:"5vh"}}>
                    <h1>Massachusetts Institute of Technology</h1>
                    <h6>Location: Cambridge, Massachusetts</h6>
                    <h6>Student Population: 11,376</h6>
                    <h6>Graduation Rate: 92.7%</h6>
                    <h6>In-state Tuition: $53,790</h6>
                    <h6>Out-of-State Tuition: $53,790</h6>
            </div>
        </div>
    )
}