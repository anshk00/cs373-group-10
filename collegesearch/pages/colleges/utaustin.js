import dynamic from "next/dynamic"

const CollegeNavBar = dynamic(
    () => import('../../components/collegeNavBar')
)

export default function Colleges() {
    return (
        <div>
            <CollegeNavBar></CollegeNavBar>
            <div className="container" style={{marginTop:"5vh"}}>
                    <h1>University of Texas at Austin</h1>
                    <h6>Location: Austin, Texas</h6>
                    <h6>Student Population: 50,950</h6>
                    <h6>Graduation Rate: 80.8%</h6>
                    <h6>In-state Tuition: $10,824</h6>
                    <h6>Out-of-State Tuition: $38,326</h6>
            </div>
        </div>
    )
}