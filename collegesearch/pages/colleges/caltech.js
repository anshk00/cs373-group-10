import dynamic from "next/dynamic"

const CollegeNavBar = dynamic(
    () => import('../../components/collegeNavBar')
)

export default function Colleges() {
    return (
        <div>
            <CollegeNavBar></CollegeNavBar>
            <div className="container" style={{marginTop:"5vh"}}>
                    <h1>California Institute of Technology</h1>
                    <h6>Location: Pasadena, California</h6>
                    <h6>Student Population: 2,238</h6>
                    <h6>Graduation Rate: 93.2%</h6>
                    <h6>In-state Tuition: $54,600</h6>
                    <h6>Out-of-State Tuition: $54,600</h6>
            </div>
        </div>
    )
}