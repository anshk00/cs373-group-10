import dynamic from "next/dynamic"

const CollegeNavBar = dynamic(
    () => import('../../components/collegeNavBar')
)

export default function Creators() {
    return (
        <div>
            <CollegeNavBar></CollegeNavBar>
            <p>this is where we show our group and git commits and all that</p>
        </div>
    )
}