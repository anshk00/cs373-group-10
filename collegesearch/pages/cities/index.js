import dynamic from "next/dynamic"

const CollegeNavBar = dynamic(
    () => import('../../components/collegeNavBar')
)

export default function Cities() {
    return (
        <div>
            <CollegeNavBar></CollegeNavBar>
            <div className="container" style={{marginTop:"5vh"}}>
                <div className="row justify-content-center">
                    <h1>Cities</h1>
                </div>
                <div className="row justify-content-center">
                    <a href="/cities/austin">Austin</a>
                </div>
                <div className="row justify-content-center">
                    <a href="/cities/cambridge">Cambridge</a>
                </div>
                <div className="row justify-content-center">
                    <a href="/cities/pasadena">Pasadena</a>
                </div>
            </div>
        </div>
    )
}