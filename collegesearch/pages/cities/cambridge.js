import dynamic from "next/dynamic"

const CollegeNavBar = dynamic(
    () => import('../../components/collegeNavBar')
)

export default function Cities() {
    return (
        <div>
            <CollegeNavBar></CollegeNavBar>
            <div className="container" style={{marginTop:"5vh"}}>
                <h1>Cambridge, Massachusetts</h1>
                <h6>Coordinates: 42.3736° N, 71.1097° W</h6>
                <h6>College: Massachusetts Institute of Technology</h6>
                <h6>Population: 118,977</h6>
                <h6>Crime Rate: 2.9 per 1000 residents</h6>
                <h6>Per Capita Income: $47,448</h6>
            </div>
        </div>
    )
}