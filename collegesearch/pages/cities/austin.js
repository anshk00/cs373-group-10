import dynamic from "next/dynamic"

const CollegeNavBar = dynamic(
    () => import('../../components/collegeNavBar')
)

export default function Cities() {
    return (
        <div>
            <CollegeNavBar></CollegeNavBar>
            <div className="container" style={{marginTop:"5vh"}}>
                <h1>Austin, Texas</h1>
                <h6>Coordinates: 30.2672° N, 97.7431° W</h6>
                <h6>College: University of Texas at Austin</h6>
                <h6>Population: 964,254</h6>
                <h6>Crime Rate: 39 per 1000 residents</h6>
                <h6>Per Capita Income: $40,087</h6>
            </div>
        </div>
    )
}