import dynamic from "next/dynamic"

const CollegeNavBar = dynamic(
    () => import('../../components/collegeNavBar')
)

export default function Cities() {
    return (
        <div>
            <CollegeNavBar></CollegeNavBar>
            <div className="container" style={{marginTop:"5vh"}}>
                <h1>Pasadena, California</h1>
                <h6>Coordinates: 34.1478° N, 118.1445° W</h6>
                <h6>College: California Institute of Technology</h6>
                <h6>Population: 138,101</h6>
                <h6>Crime Rate: 3.8 per 1000 residents</h6>
                <h6>Per Capita Income: $44,263</h6>
            </div>
        </div>
    )
}