import Head from 'next/head'
import dynamic from 'next/dynamic' 
import { InputGroup, DropdownButton, Dropdown, FormControl } from 'react-bootstrap'

const CollegeNavBar = dynamic(
  () => import('../components/collegeNavBar')
)

export default function Home() {
  return (
    <div className="" style={{"height":"100vh"}}>
      <Head>
        <title>collegesearch.me</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <CollegeNavBar></CollegeNavBar>
      <div className="container" style={{marginTop:"15%"}}>
        <div className="row justify-content-center">
          <h1>collegesearch.me</h1>
        </div>
        <div className="row justify-content-center">
          <h5>find the best college for you.</h5>
        </div>
        <div className="row mt-3" style={{"width":"500px", marginLeft:"20vw"}}>
          <InputGroup>
            <DropdownButton
              as={InputGroup.Prepend}
              variant="outline-secondary"
              title="Dropdown">
                <Dropdown.Item href="#">College</Dropdown.Item>
                <Dropdown.Item href="#">City</Dropdown.Item>
                <Dropdown.Item href="#">Admissions</Dropdown.Item>
            </DropdownButton>
            <FormControl aria-describedby="basic-addon1"/>
          </InputGroup>
        </div>
      </div>
    </div>
  )
}
